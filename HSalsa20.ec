require import AllCore Int IntDiv List.
require import FinType Group Distr DList.
require (*--*) Word.

(** A Finite type for words with a group operation **)
type w.
op ('0) : w.
op (+)  : w -> w -> w.
op [-]  : w -> w.

abbrev (-) w1 w2 = w1 + -w2.

axiom addwC : commutative (+).
axiom add0w : left_id '0 (+).
axiom addwA : associative (+).
axiom addKw : left_inverse '0 [-] (+).

clone import ComGroup with
   type group <- w,
     op     e <- '0,
     op ( * ) <- (+),
     op   inv <- [-],
  lemma mulcC <- addwC,
  lemma mul1c <- add0w,
  lemma mulcA <- addwA,
  lemma mulVc <- addKw
rename "mul1" as "add0"
       "mul"  as "add".

clone FinType as FinW with
  type t <= w.

clone MFinite as DW with
    type       t <= w,
  theory Support <= FinW.
op dW = DW.dunifin.

(** Salsa operates on strings of words of lengths 4, 8, 16 **)
clone import Word as K with
  theory ListSample.DAlphabet <= DW,
  theory Alphabet <- FinW,
      op        n <- 4
proof ge0_n by done
rename "DWord" as "DK"
       "word" as "K"
       "dunifin" as "dK".
import DK.

clone import Word as I with
  theory ListSample.DAlphabet <= DW,
  theory Alphabet <- FinW,
      op        n <- 16
proof ge0_n by done
rename "DWord" as "DI"
       "word" as "I"
       "dunifin" as "dI".
import DI.

clone import Word as O with
  theory ListSample.DAlphabet <= DW,
  theory Alphabet <- FinW,
      op        n <- 8
proof ge0_n by done
rename "DWord" as "DO"
       "word" as "O"
       "dunifin" as "dO".
import DO.

(** Operators **)
op r : { int | 0 <= r /\ r %% 2 = 0 } as rP.

op sc : K.
op doubleround : I -> I.

op salsa20 (k : K) (nc : O) =
  let x = mkI
            [sc.[0]; 
             k.[0]; k.[1]; k.[2]; k.[3];
             sc.[1];
             nc.[0]; nc.[1];
             nc.[2]; nc.[3];
             sc.[2];
             k.[4]; k.[5]; k.[6]; k.[7];
             sc.[3]] in
  let z = iter (r %/ 2) doubleround x in
  mkI (map (fun i=> x.[i] + z.[i]) (range 0 16)).

op hsalsa20 (k : K) (n : O) =
  let x = mkI
            [sc.[0];
             k.[0]; k.[1]; k.[2]; k.[3];
             sc.[1];
             n.[0]; n.[1]; n.[2]; n.[3];
             sc.[2];
             k.[4]; k.[5]; k.[6]; k.[7];
             sc.[3]] in
  let z = iter (r %/ 2) doubleround x in
  mkO [z.[0]; z.[5]; z.[10]; z.[15]; z.[6]; z.[7]; z.[8]; z.[9]].

op q (o : O) (i : I) =
  mkO
    [i.[0] - sc.[0]; i.[5] - sc.[1]; i.[10] - sc.[2]; i.[15] - sc.[3];
     i.[6] - o.[0]; i.[7] - o.[1]; i.[8] - o.[2]; i.[9] - o.[3]].

lemma hsalsa_q k i:
  hsalsa20 k i = q i (salsa20 k i).
proof.
pose x:=  mkI [sc.[0]; k.[0]; k.[1]; k.[2]; k.[3]; sc.[1]; i.[0]; i.[1]; i.[2]; i.[3]; sc.[2]; k.[4]; k.[5]; k.[6]; k.[7]; sc.[3]].
pose z:= iter (r %/ 2) doubleround x.
have ->: hsalsa20 k i = mkO [z.[0]; z.[5]; z.[10]; z.[15]; z.[6]; z.[7]; z.[8]; z.[9]] by done.
have ->: salsa20 k i = mkI (map (fun i=> x.[i] + z.[i]) (range 0 16)) by done.
rewrite /q !(I.getE (mkI _)) /= !ofIK 1:size_map 1:size_range 1:/#.
rewrite !(nth_map witness) 1..8:size_range 1..8:/# //=.
rewrite !nth_range //=.
rewrite !(I.getE (mkI _)) /= !ofIK //=.
have addwNK: forall w w', w + w' - w = w'.
+ by move=> w w'; rewrite (addwC w) -addwA (addwC w) addKw addc1.
by rewrite !addwNK.
qed.

theory HSalsa_Security.
require import FMap.
require (*--*) PRF.

(** We show that HSalsa20 is a secure PRF if Salsa20 is a secure PRF **)
clone PRF as H_PRF_t with
  type D <- O,
  type R <- O
proof *.

clone H_PRF_t.RF as H_RF with
  op dR _ <- dO
proof *.
realize dR_ll. by move=> _; exact: dO_ll.
qed.

clone H_PRF_t.PseudoRF as H_PRF with
  type  K <- K,
    op dK <- dK,
    op  F <- hsalsa20
proof *.
realize dK_ll. exact: dK_ll.
qed.

module HRF      = H_RF.RF.
module HSalsa20 = H_PRF.PRF.

clone PRF as S_PRF_t with
  type D <- O,
  type R <- I
proof *.

clone S_PRF_t.RF as S_RF with
  op dR _ <- dI
proof *.
realize dR_ll. by move=> _; exact: dI_ll.
qed.

clone S_PRF_t.PseudoRF as S_PRF with
  type  K <- K,
    op dK <- dK,
    op  F <- salsa20
proof *.
realize dK_ll. exact: dK_ll.
qed.

module SRF     = S_RF.RF.
module Salsa20 = S_PRF.PRF.

module (QD (D : H_PRF_t.Distinguisher) : S_PRF_t.Distinguisher) (S : S_PRF_t.PRF_Oracles) = {
  module Q = {
    proc f(x) = {
      var i;
  
      i <@ S.f(x);
      return q x i;
    }
  }

  proc distinguish = D(Q).distinguish
}.

section Proof.
declare module D <: H_PRF_t.Distinguisher { -SRF, -Salsa20, -HRF, -HSalsa20 }.

local op q' (o0 o1 : O) =
  mkO [o1.[0] - sc.[0]; o1.[1] - sc.[1]; o1.[6] - sc.[2]; o1.[7] - sc.[3];
       o1.[2] - o0.[0]; o1.[3] - o0.[1]; o1.[4] - o0.[2]; o1.[5] - o0.[3]].

local op unq' (o0 o2 : O) =
  mkO [o2.[0] + sc.[0]; o2.[1] + sc.[1]; o2.[4] + o0.[0]; o2.[5] + o0.[1];
       o2.[6] + o0.[2]; o2.[7] + o0.[3]; o2.[2] + sc.[2]; o2.[3] + sc.[3]].

local lemma unq'K (o0 o1 : O):
  unq' o0 (q' o0 o1) = o1.
proof.
rewrite /q' /unq' !O.getE //= !ofOK //=.
rewrite -!addwA !addVc !addc1.
rewrite OP=> i i_bounds; rewrite !O.getE i_bounds //=.
by rewrite ofOK //#.
qed.

local lemma q'K (o0 o1 : O):
  q' o0 (unq' o0 o1) = o1.
proof.
rewrite /q' /unq' !O.getE //= !ofOK //=.
rewrite -!addwA !addcV !addc1.
rewrite OP=> i i_bounds; rewrite !O.getE i_bounds //=.
by rewrite ofOK //#.
qed.

local lemma q_q' (o0 o1 o2 : O):
    q o0 (mkI [o1.[0]; o2.[0]; o2.[1]; o2.[2]; o2.[3]; o1.[1]; o1.[2]; o1.[3];
               o1.[4]; o1.[5]; o1.[6]; o2.[4]; o2.[5]; o2.[6]; o2.[7]; o1.[7]])
  = q' o0 o1.
proof. by rewrite /q' /q !I.getE //= !ofIK. qed.

local clone DList.Program as SWord with
  type t <- w,
    op d <- dW.

lemma HSalsa20_Security &m:
     `|  Pr[H_PRF_t.IND(HSalsa20, D).main() @ &m: res]
       - Pr[H_PRF_t.IND(HRF     , D).main() @ &m: res]|
  <= `|  Pr[S_PRF_t.IND(Salsa20, QD(D)).main() @ &m: res]
       - Pr[S_PRF_t.IND(SRF    , QD(D)).main() @ &m: res]|.
proof.
have ->: Pr[H_PRF_t.IND(HSalsa20, D).main() @ &m: res]
       = Pr[S_PRF_t.IND(Salsa20, QD(D)).main() @ &m: res].
+ byequiv (: ={glob D} ==> ={res})=> //.
  proc; inline *.
  call (: H_PRF.PRF.k{1} = S_PRF.PRF.k{2}); auto.
  by proc; inline *; auto=> /> &2; rewrite hsalsa_q.
have -> //: Pr[H_PRF_t.IND(HRF, D).main() @ &m: res]
          = Pr[S_PRF_t.IND(SRF, QD(D)).main() @ &m: res].
byequiv (: ={glob D} ==> ={res})=> //.
proc; inline *.
call (: H_RF.RF.m{1} = map q S_RF.RF.m{2}); last first.
+ by auto=> />; rewrite map_empty.
proc; inline *.
sp; if; last first.
+ by auto=> /> &2; rewrite mem_map domE mapE; case: (S_RF.RF.m.[x]{2}).
+ by move=> />; rewrite mem_map.
auto=> />.
conseq (: r{1} = q x{2} r{2})=> //=.
+ move=> /> &2; rewrite mem_map domE=> /= + i.
  by rewrite !get_set_sameE /= map_set.
alias {1} 1 pr = [<:w>].
transitivity {1}
  { r <$ dO; }
  (={x} ==> r{1} = q' x{2} r{2})
  (={x} ==> q' x{1} r{1} = q x{2} r{2})=> />.
+ by move=> &2 _; exists x{2}.
+ rnd (unq' x{1}) (q' x{1}); auto=> /> &2; split.
  + by move=> r _; rewrite unq'K.
  by move=> _ r _; rewrite q'K.
alias {1} 1 r' = r.
transitivity * {1} { r' <$ dO; r <$ dO; }=> />.
+ by move=> &1 &2; exists r{1} x{2}.
+ by rnd; rnd {2} dO_ll; auto.
transitivity {1}
  { r' <$ dO; r <$ dO; }
  (={x} ==> q' x{2} r{1} = q x{2} (mkI [r.[0]; r'.[0]; r'.[1]; r'.[2]; r'.[3]; r.[1]; r.[2]; r.[3];
                                        r.[4]; r.[5]; r.[6]; r'.[4]; r'.[5]; r'.[6]; r'.[7]; r.[7]]){2}
         /\ ={x})
  (={x} ==> (mkI [r.[0]; r'.[0]; r'.[1]; r'.[2]; r'.[3]; r.[1]; r.[2]; r.[3];
                  r.[4]; r.[5]; r.[6]; r'.[4]; r'.[5]; r'.[6]; r'.[7]; r.[7]]){1} = r{2}
         /\ ={x})=> />.
+ by move=> &2; exists x{2}.
+ conseq (: ={r})=> //=.
  + by move=> /> &2 r r'; rewrite q_q'.
  by auto.
(** WHY IS THIS SO HARD??? **)
conseq (: [r.[0]; r'.[0]; r'.[1]; r'.[2]; r'.[3]; r.[1]; r.[2]; r.[3];
           r.[4]; r.[5]; r.[6]; r'.[4]; r'.[5]; r'.[6]; r'.[7]; r.[7]]{1}
        = [r.[0]; r.[1]; r.[2]; r.[3]; r.[4]; r.[5]; r.[6]; r.[7];
           r.[8]; r.[9]; r.[10]; r.[11]; r.[12]; r.[13]; r.[14]; r.[15]]{2}).
+ move=> &1 &2 _ rO rO' rI /> 16!->.
  rewrite -{17}(mkIK rI); congr; apply: (eq_from_nth witness).
  + by rewrite size_I.
  move=> //= i bnd_i; rewrite !I.getE.
  smt(nth_out).
transitivity {1} { pr <$ dlist dW 8; r' <- mkO pr;
                   pr <$ dlist dW 8; r  <- mkO pr; }
  (true ==> ={r, r'})
  (true ==> [r.[0]; r'.[0]; r'.[1]; r'.[2]; r'.[3]; r.[1]; r.[2]; r.[3];
             r.[4]; r.[5]; r.[6]; r'.[4]; r'.[5]; r'.[6]; r'.[7]; r.[7]]{1}
          = [r.[0]; r.[1]; r.[2]; r.[3]; r.[4]; r.[5]; r.[6]; r.[7];
             r.[8]; r.[9]; r.[10]; r.[11]; r.[12]; r.[13]; r.[14]; r.[15]]{2})=> //.
+ wp; rnd ofO mkO.
  wp; rnd ofO mkO.
  auto=> />; split=> [|_].
  + by move=> ws; rewrite supp_dlist=> /> /ofOK ->.
  split=> [|_].
  + move=> ws ws_in_dW8.
    rewrite -ListSample.dO_is_dlist (in_dmap1E_can _ _ ofO).
    + rewrite ofOK //.
      by move: ws_in_dW8; rewrite supp_dlist.
    + move=> w'; rewrite supp_dlist=> /> size_w' _ <-.
      by rewrite ofOK.
    rewrite ofOK //.
    by move: ws_in_dW8; rewrite supp_dlist.
  move=> r' r'_in_dO; rewrite mkOK /=.
  rewrite supp_dlist // size_O //= allP.
  split=> [|_].
  + by move=> w _; rewrite DW.dunifin_fu.
  move=> r r_in_dO; rewrite mkOK /=.
  rewrite supp_dlist // size_O //= allP.
  by move=> x; rewrite DW.dunifin_fu.
transitivity {1} { pr <@ SWord.Loop.sample(8); r' <- mkO pr;
                   pr <@ SWord.Loop.sample(8); r  <- mkO pr; }
  (true ==> ={r, r'})
  (true ==> [r.[0]; r'.[0]; r'.[1]; r'.[2]; r'.[3]; r.[1]; r.[2]; r.[3];
             r.[4]; r.[5]; r.[6]; r'.[4]; r'.[5]; r'.[6]; r'.[7]; r.[7]]{1}
          = [r.[0]; r.[1]; r.[2]; r.[3]; r.[4]; r.[5]; r.[6]; r.[7];
             r.[8]; r.[9]; r.[10]; r.[11]; r.[12]; r.[13]; r.[14]; r.[15]]{2})=> //.
+ seq  2  2: (={r'}).
  + outline {1} [1] pr <@ SWord.Sample.sample.
    by wp; call SWord.Sample_Loop_eq.
  outline {1} [1] pr <@ SWord.Sample.sample.
  by wp; call SWord.Sample_Loop_eq.
alias {2} 0 pr = witness<:w list>.
transitivity {2} { pr <$ dlist dW 16; r <- mkI pr; }
  (true ==> [r.[0]; r'.[0]; r'.[1]; r'.[2]; r'.[3]; r.[1]; r.[2]; r.[3];
             r.[4]; r.[5]; r.[6]; r'.[4]; r'.[5]; r'.[6]; r'.[7]; r.[7]]{1}
          = [r.[0]; r.[1]; r.[2]; r.[3]; r.[4]; r.[5]; r.[6]; r.[7];
             r.[8]; r.[9]; r.[10]; r.[11]; r.[12]; r.[13]; r.[14]; r.[15]]{2})
  (true ==> ={r})=> //; last first.
+ symmetry; wp; rnd ofI mkI.
  auto=> />; split=> [|_].
  + by move=> ws; rewrite supp_dlist=> /> /ofIK ->.
  split=> [|_].
  + move=> ws ws_in_dW8.
    rewrite -I.ListSample.dI_is_dlist (in_dmap1E_can _ _ ofI).
    + rewrite ofIK //.
      by move: ws_in_dW8; rewrite supp_dlist.
    + move=> w'; rewrite supp_dlist=> /> size_w' _ <-.
      by rewrite ofIK.
    rewrite ofIK //.
    by move: ws_in_dW8; rewrite supp_dlist.
  move=> r' r'_in_dO; rewrite mkIK /=.
  rewrite supp_dlist // size_I //= allP.
  by move=> w _; rewrite DW.dunifin_fu.
transitivity {2} { pr <@ SWord.Loop.sample(16); r <- mkI pr; }
  (true ==> [r.[0]; r'.[0]; r'.[1]; r'.[2]; r'.[3]; r.[1]; r.[2]; r.[3];
             r.[4]; r.[5]; r.[6]; r'.[4]; r'.[5]; r'.[6]; r'.[7]; r.[7]]{1}
          = [r.[0]; r.[1]; r.[2]; r.[3]; r.[4]; r.[5]; r.[6]; r.[7];
             r.[8]; r.[9]; r.[10]; r.[11]; r.[12]; r.[13]; r.[14]; r.[15]]{2})
  (true ==> ={r})=> //; last first.
+ outline {2} [1] pr <@ SWord.Sample.sample.
  by symmetry; wp; call SWord.Sample_Loop_eq.
alias {1} 0 r'7 = witness<:w>.
alias {1} 0 r'6 = witness<:w>.
alias {1} 0 r'5 = witness<:w>.
alias {1} 0 r'4 = witness<:w>.
alias {1} 0 r'3 = witness<:w>.
alias {1} 0 r'2 = witness<:w>.
alias {1} 0 r'1 = witness<:w>.
alias {1} 0 r'0 = witness<:w>.
alias {1} 0  r7 = witness<:w>.
alias {1} 0  r6 = witness<:w>.
alias {1} 0  r5 = witness<:w>.
alias {1} 0  r4 = witness<:w>.
alias {1} 0  r3 = witness<:w>.
alias {1} 0  r2 = witness<:w>.
alias {1} 0  r1 = witness<:w>.
alias {1} 0  r0 = witness<:w>.
seq  16 0: true; 1:by auto.
transitivity {1}
  { r'7 <$ dW; r'6 <$ dW; r'5 <$ dW; r'4 <$ dW; r'3 <$ dW; r'2 <$ dW; r'1 <$ dW; r'0 <$ dW;
    r7  <$ dW; r6  <$ dW; r5  <$ dW; r4  <$ dW; r3  <$ dW; r2  <$ dW; r1  <$ dW; r0  <$ dW; }
  (true ==> r.[0]{1}  = r0{2}
         /\ r.[1]{1}  = r1{2}
         /\ r.[2]{1}  = r2{2}
         /\ r.[3]{1}  = r3{2}
         /\ r.[4]{1}  = r4{2}
         /\ r.[5]{1}  = r5{2}
         /\ r.[6]{1}  = r6{2}
         /\ r.[7]{1}  = r7{2}
         /\ r'.[0]{1} = r'0{2}
         /\ r'.[1]{1} = r'1{2}
         /\ r'.[2]{1} = r'2{2}
         /\ r'.[3]{1} = r'3{2}
         /\ r'.[4]{1} = r'4{2}
         /\ r'.[5]{1} = r'5{2}
         /\ r'.[6]{1} = r'6{2}
         /\ r'.[7]{1} = r'7{2})
  (true ==> [r0; r'0; r'1; r'2; r'3; r1; r2; r3;
             r4; r5; r6; r'4; r'5; r'6; r'7; r7]{1}
          = [r.[0]; r.[1]; r.[2]; r.[3]; r.[4]; r.[5]; r.[6]; r.[7];
             r.[8]; r.[9]; r.[10]; r.[11]; r.[12]; r.[13]; r.[14]; r.[15]]{2})=> //.
+ inline *.
  rcondt {1}  4; 1:by auto.
  rcondt {1}  7; 1:by auto.
  rcondt {1} 10; 1:by auto.
  rcondt {1} 13; 1:by auto.
  rcondt {1} 16; 1:by auto.
  rcondt {1} 19; 1:by auto.
  rcondt {1} 22; 1:by auto.
  rcondt {1} 25; 1:by auto.
  rcondf {1} 28; 1:by auto.
  rcondt {1} 33; 1:by auto.
  rcondt {1} 36; 1:by auto.
  rcondt {1} 39; 1:by auto.
  rcondt {1} 42; 1:by auto.
  rcondt {1} 45; 1:by auto.
  rcondt {1} 48; 1:by auto.
  rcondt {1} 51; 1:by auto.
  rcondt {1} 54; 1:by auto.
  rcondf {1} 57; 1:by auto.
  auto=> />.
  auto=> /> r15 _ r14 _ r13 _ r12 _ r11 _ r10 _ r9 _ r8 _ r7 _.
  move=> r6 _ r5 _ r4 _ r3 _ r2 _ r1 _ r0 _.
  by rewrite !/(_.[_])%O //= !ofOK.
alias {2} 0 r15 = witness<:w>.
alias {2} 0 r14 = witness<:w>.
alias {2} 0 r13 = witness<:w>.
alias {2} 0 r12 = witness<:w>.
alias {2} 0 r11 = witness<:w>.
alias {2} 0 r10 = witness<:w>.
alias {2} 0  r9 = witness<:w>.
alias {2} 0  r8 = witness<:w>.
alias {2} 0  r7 = witness<:w>.
alias {2} 0  r6 = witness<:w>.
alias {2} 0  r5 = witness<:w>.
alias {2} 0  r4 = witness<:w>.
alias {2} 0  r3 = witness<:w>.
alias {2} 0  r2 = witness<:w>.
alias {2} 0  r1 = witness<:w>.
alias {2} 0  r0 = witness<:w>.
seq  0 16: true; 1:by auto.
transitivity {2} 
  { r15 <$ dW; r14 <$ dW; r13 <$ dW; r12 <$ dW; r11 <$ dW; r10 <$ dW; r9  <$ dW; r8  <$ dW;
    r7  <$ dW; r6  <$ dW; r5  <$ dW; r4  <$ dW; r3  <$ dW; r2  <$ dW; r1  <$ dW; r0  <$ dW; }
  (true ==> [r0; r'0; r'1; r'2; r'3; r1; r2; r3;
             r4; r5; r6; r'4; r'5; r'6; r'7; r7]{1}
          = [r0; r1; r2; r3; r4; r5; r6; r7;
             r8; r9; r10; r11; r12; r13; r14; r15]{2})
  (true ==> r0{1}  = r.[0]{2}
         /\ r1{1}  = r.[1]{2}
         /\ r2{1}  = r.[2]{2}
         /\ r3{1}  = r.[3]{2}
         /\ r4{1}  = r.[4]{2}
         /\ r5{1}  = r.[5]{2}
         /\ r6{1}  = r.[6]{2}
         /\ r7{1}  = r.[7]{2}
         /\ r8{1}  = r.[8]{2}
         /\ r9{1}  = r.[9]{2}
         /\ r10{1} = r.[10]{2}
         /\ r11{1} = r.[11]{2}
         /\ r12{1} = r.[12]{2}
         /\ r13{1} = r.[13]{2}
         /\ r14{1} = r.[14]{2}
         /\ r15{1} = r.[15]{2})=> //.
+ swap {1}  8  7.
  swap {1}  7  7.
  swap {1}  6  7.
  swap {1}  5  7.
  swap {1}  5  -4.
  by auto.
inline *.
rcondt {2}  4; 1:by auto.
rcondt {2}  7; 1:by auto.
rcondt {2} 10; 1:by auto.
rcondt {2} 13; 1:by auto.
rcondt {2} 16; 1:by auto.
rcondt {2} 19; 1:by auto.
rcondt {2} 22; 1:by auto.
rcondt {2} 25; 1:by auto.
rcondt {2} 28; 1:by auto.
rcondt {2} 31; 1:by auto.
rcondt {2} 34; 1:by auto.
rcondt {2} 37; 1:by auto.
rcondt {2} 40; 1:by auto.
rcondt {2} 43; 1:by auto.
rcondt {2} 46; 1:by auto.
rcondt {2} 49; 1:by auto.
rcondf {2} 52; 1:by auto.
auto=> /> r15 _ r14 _ r13 _ r12 _ r11 _ r10 _ r9 _ r8 _.
move=> r7 _ r6 _ r5 _ r4 _ r3 _ r2 _ r1 _ r0 _.
by rewrite !getE //= !ofIK.
qed.

end section Proof.

end HSalsa_Security.
