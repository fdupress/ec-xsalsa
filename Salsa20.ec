require import AllCore List Int IntDiv Ring.
require import BitEncoding.
require (*--*) BitWord Word.

(*---*) import BitEncoding.BS2Int IntID.

clone import BitWord as Word32 with
     op                  n <- 32
proof gt0_n by done
rename "word" as "word32".

(** TODO: Check endianness and stick a rev in if needed.
    Salsa spec says words are little-endian bitstrings
**)
op w2int (w : word32): int = bs2int (ofword32 w).
op int2w (i : int): word32 = mkword32 (int2bs 32 i).

lemma w2int_ge0 w: 0 <= w2int w.
proof. by rewrite bs2int_ge0. qed.

lemma w2intK w: int2w (w2int w) = w.
proof. by rewrite /w2int /int2w -(size_word32 w) bs2intK mkword32K. qed.

lemma int2wK i: 0 <= i < 2 ^ 32 => w2int (int2w i) = i.
proof.
move=> i_mod2X32 @/w2int @/int2w.
by rewrite ofword32K 1:size_int2bs 2:int2bsK.
qed.

lemma int2wK_mod i: 0 <= i => w2int (int2w i) = i %% 2 ^ 32.
proof.
move=> ge0_i @/w2int @/int2w; rewrite ofword32K 1:size_int2bs //.
rewrite -int2bs_mod int2bsK //.
split=> [|_].
+ apply: modz_ge0=> //.
  exact/StdOrder.IntOrder.ltr0_neq0/StdOrder.IntOrder.expr_gt0.
exact/ltz_pmod/StdOrder.IntOrder.expr_gt0.
qed.

(** Addition mod 2^32 **)
op (+) (w1 w2 : word32): word32 = int2w (w2int w1 + w2int w2).

lemma w32_addP w1 w2: w2int (w1 + w2) = (w2int w1 + w2int w2) %% 2 ^ 32.
proof. by apply/int2wK_mod/StdOrder.IntOrder.addr_ge0; exact: w2int_ge0. qed.

clone import Word as Word64 with
   type         Alphabet.t <- word32,
     op      Alphabet.enum <- word32s,
  lemma Alphabet.enum_spec <- Word32.enum_spec,
     op                  n <- 2
proof ge0_n by done
rename "word" as "word64".

clone import Word as Word128 with
   type         Alphabet.t <- word32,
     op      Alphabet.enum <- word32s,
  lemma Alphabet.enum_spec <- Word32.enum_spec,
     op                  n <- 4
proof ge0_n by done
rename "word" as "word128".

clone import Word as Word192 with
   type         Alphabet.t <- word32,
     op      Alphabet.enum <- word32s,
  lemma Alphabet.enum_spec <- Word32.enum_spec,
     op                  n <- 6
proof ge0_n by done
rename "word" as "word192".

clone import Word as Word256 with
   type         Alphabet.t <- word32,
     op      Alphabet.enum <- word32s,
  lemma Alphabet.enum_spec <- Word32.enum_spec,
     op                  n <- 8
proof ge0_n by done
rename "word" as "word256".

clone import Word as Word512 with
   type         Alphabet.t <- word32,
     op      Alphabet.enum <- word32s,
  lemma Alphabet.enum_spec <- Word32.enum_spec,
     op                  n <- 16
proof ge0_n by done
rename "word" as "word512".

(** Parameter and abstraction boundaries **)
op r : { int | 0 <= r /\ r %% 2 = 0 } as r_valid.

op doubleround: word512 -> word512.
op s20c: word128.

(** NOTE: The nonce and counter size are different in the standard **)
(** TODO: Figure out:
    - How can we parameterize this when the weaving is so precise?
    - Do we need to capture the weaving so precisely? (Could we
      abstract it away as a `weave' operator?)
**)
op salsa20 (k : word256) (n : word64) (c : word64): word512 =
  let x = mkword512
            [s20c.[0];
             k.[0]; k.[1]; k.[2]; k.[3];
             s20c.[1];
             n.[0]; n.[1];
             c.[0]; c.[1];
             s20c.[2];
             k.[4]; k.[5]; k.[6]; k.[7];
             s20c.[3]] in
  let z = iter (r %/ 2) doubleround x in
  mkword512 (map (fun i=> x.[i] + z.[i]) (range 0 16)).

op hsalsa20 (k : word256) (n : word128): word256 =
  let x = mkword512
            [s20c.[0];
             k.[0]; k.[1]; k.[2]; k.[3];
             s20c.[1];
             n.[0]; n.[1]; n.[2]; n.[3];
             s20c.[2];
             k.[4]; k.[5]; k.[6]; k.[7];
             s20c.[3]] in
  let z = iter (r %/ 2) doubleround x in
  mkword256 [z.[0]; z.[5]; z.[10]; z.[15]; z.[6]; z.[7]; z.[8]; z.[9]].

op xsalsa20 (k : word256) (n : word192) (c : word64): word512 =
  let x  = mkword512
             [s20c.[0];
              k.[0]; k.[1]; k.[2]; k.[3];
              s20c.[1];
              n.[0]; n.[1]; n.[2]; n.[3];
              s20c.[2];
              k.[4]; k.[5]; k.[6]; k.[7];
              s20c.[3]] in
  let z  = iter (r %/ 2) doubleround x in
  let x' = mkword512
             [s20c.[0];
              z.[0]; z.[5]; z.[10]; z.[15];
              s20c.[1];
              n.[4]; n.[5];
              c.[0]; c.[1];
              s20c.[2];
              z.[6]; z.[7]; z.[8]; z.[9];
              s20c.[3]] in
  let z' = iter (r %/ 2) doubleround x' in
  mkword512 (map (fun i=> x'.[i] + z'.[i]) (range 0 16)).

lemma xsalsa_decomp k n c:
    xsalsa20 k n c
  = salsa20 (hsalsa20 k (mkword128 [n.[0]; n.[1]; n.[2]; n.[3]])) (mkword64 [n.[4]; n.[5]]) c.
proof.
rewrite /xsalsa20 /salsa20 //=.
congr; apply: eq_in_map=> i /mem_range i_bd //=.
pose x := mkword512 ([_; k.[0]; _; _; _; _; _; _; _; _; _; _; _; _; _; _]).
pose k1 := iter (r %/ 2) doubleround x.
have ->:   hsalsa20 k (mkword128 [n.[0]; n.[1]; n.[2]; n.[3]])
         = mkword256 [k1.[0]; k1.[5]; k1.[10]; k1.[15]; k1.[6]; k1.[7]; k1.[8]; k1.[9]].
+ rewrite /hsalsa20.
  have lookupE: forall x i,
                      size x = 4
                   => 0 <= i < 4
                   => (mkword128 x).[i] = nth witness x i.
  + by move=> x' i' size_x' i'_bd; rewrite Word128.getE i'_bd //= ofword128K.
  by rewrite !lookupE.
have lookupE: forall x i,
                    size x = 8
                 => 0 <= i < 8
                 => (mkword256 x).[i] = nth witness x i.
+ by move=> x' i' size_x' i'_bd; rewrite Word256.getE i'_bd //= ofword256K.
rewrite !lookupE=> //= {lookupE}.
have lookupE: forall x i,
                    size x = 2
                 => 0 <= i < 2
                 => (mkword64 x).[i] = nth witness x i.
+ by move=> x' i' size_x' i'_bd; rewrite Word64.getE i'_bd //= ofword64K.
by rewrite !lookupE.
qed.

op s (k : word256) (n : word128) =
  salsa20 k (mkword64 [n.[0]; n.[1]]) (mkword64 [n.[2]; n.[3]]).

theory Security.
require (*--*) StdBigop.
(*---*) import StdBigop.Bigreal BRA.
require (*--*) Cascades.

clone import Cascades with
   type K1 <- word256,
   type K2 <- word256,
   type I1 <- word128,
   type I2 <- word128,
   type  L <- word512,
     op  h <- hsalsa20,
     op  s <- s,
     op Uni_dK1.K1_Fin.enum      <- word256s,
  lemma Uni_dK1.K1_Fin.enum_spec <- Word256.enum_spec,
     op Uni_dK2.K2_Fin.enum      <- word256s,
  lemma Uni_dK2.K2_Fin.enum_spec <- Word256.enum_spec,
     op   Uni_dL.L_Fin.enum      <- word512s,
  lemma   Uni_dL.L_Fin.enum_spec <- Word512.enum_spec
proof *.

section.
declare op q : { int | 1 <= q } as ge1_q.

declare module D <: X_PRF_t.Distinguisher { -XSalsa20, -XRF, -HSalsa20, -HRF, -Salsa20, -SRF, -Dj, -QCount }.

declare axiom X_f_ll (X <: X_PRF_t.PRF_Oracles { -D }):
     islossless X.f
  => islossless D(X).distinguish.

declare axiom X_QCount c (X <: X_PRF_t.PRF { -QCount, -D }):
  hoare [D(QCount(X)).distinguish:
          QCount.c = c ==> QCount.c <= c + q].

lemma XSalsa20_Security &m:
     `|  Pr[X_PRF_t.IND(XSalsa20, D).main() @ &m: res]
       - Pr[X_PRF_t.IND(XRF, D).main() @ &m: res]|
  <=   `|  Pr[H_PRF_t.IND(HSalsa20, D0(D)).main() @ &m: res]
         - Pr[H_PRF_t.IND(HRF, D0(D)).main() @ &m: res]|
     + bigi predT
         (fun i=> `|  Pr[S_IND_j(Salsa20, Dj(D)).main(i) @ &m: res]
                    - Pr[S_IND_j(SRF, Dj(D)).main(i) @ &m: res]|)
         1 (q + 1).
proof.
exact: (Cascade_Security q ge1_q D X_f_ll X_QCount &m).
qed.

end section.

end Security.
